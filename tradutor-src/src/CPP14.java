import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CPP14 {
	static String input="";
	static String output="";
	public static void main(String[] args) {
		System.out.println("test");
		getInput();
		generateOutput();
		System.out.println(".");
	}
	static void getInput(){
		Scanner in = new Scanner(System.in);
		System.out.println("c++ file path:");
		String s = in.nextLine();
		in.close();
		try {
			input = readFile(s);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("err input");
		}	
	}
	
	private static String readFile(String pathname) throws IOException {

	    File file = new File(pathname);
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}
	static void generateOutput(){
		CPP14Lexer lexer = new CPP14Lexer(new ANTLRInputStream(input));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CPP14Parser parser = new CPP14Parser(tokens);
		parser.setBuildParseTree(true);
		ParseTree tree = parser.translationunit();
		ParseTreeWalker walker = new ParseTreeWalker();
		CPP14Walker converter =  new CPP14Walker();
		walker.walk( converter , tree );
		
		try {
			File file = new File("output.java");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(converter.translatedSource);
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("translation:");
		System.out.println(converter.translatedSource);
		System.out.println("CPP14 tree:");
		System.out.println(tree.getText());
		

	}

}
